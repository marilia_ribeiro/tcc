Comentários

- INTRODUÇÃO

- País do agronegócio (panorama atual do país)
- Pecuária de Precisão: o que é e pra que serve? 
- Como se monitora: sensores, sistemas de gestão, visão computacional.
- O que se pode monitorar
- Vantagens: produtor, financeiro, bem estar animal, pesquisa.
- Estudo de caso - IFC Araquari (análise de trajetórias)

A pecuária consiste na criação de animais para fins comerciais. O Brasil é um pais  ...

Com um mercado cada vez mais exigente surge a necessidade de conhecer melhor o animal para identificar melhores formas de criação. Nesse sentido o ramo da Pecuária de Precisão vem ganhando forças.

Nesse sentido a  A utilização de  Assim, a Pecuária de Precisão (PP)

Valor e importância da pecuária de precisão para a economia.



- CONCEITOS E DEFINIÇÕES
\begin{comment}

 - Um ponto é um par ordenado (x, y) de coordenadas espaciais. 
 - Uma linha é um conjunto de pontos conectados. 
 - Uma área (ou polígono) é a região do plano limitada por uma ou mais linhas poligonais conectadas de tal forma que o último ponto de uma linha seja idêntico ao primeiro da próxima. Observe-se também que a fronteira do polígono divide o plano em duas regiões: o interior e o exterior.


A modelagem de dados geográficos é uma
atividade complexa porque envolve a discretização do espaço como parte do
processo de abstração, visando obter representações adequadas aos fenômenos
geográficos.

O modelo OMT-G (Object Modeling Technique for Geographic Applications) parte
das primitivas definidas para o diagrama de classes da UML, introduzindo primitivas
geográficas com o objetivo de aumentar a capacidade de representação semântica do
espaço (Borges, 2001). O modelo OMT-G provê primitivas para modelar a geometria
e a topologia dos dados geográficos, oferecendo suporte a estruturas topológicas
“todo-parte”, estruturas de rede, múltiplas representações de objetos e
relacionamentos espaciais. Além disso, o modelo permite a especificação de
atributos alfanuméricos e métodos associados para cada classe.
Esse modelo é baseado em três conceitos principais: classes,
relacionamentos e restrições de integridade espaciais. Classes e relacionamentos
definem as primitivas básicas usadas para criar esquemas estáticos de aplicação.
Além disso, OMT-G propõe o uso de três diferentes diagramas no processo de
desenvolvimento de uma aplicação geográfica: diagrama de classes, diagrama de
transformação e diagrama de apresentação. O diagrama de classes é o mais usual
e contém as classes especificadas junto com suas representações e relacionamentos.
A partir do diagrama de classes é possível derivar um conjunto de restrições de
integridade espaciais, que deve ser considerado na implementação.

As classes definidas pelo modelo OMT-G representam os três grandes grupos
de dados (contínuos, discretos e não-espaciais) que podem ser encontrados nas
aplicações geográficas, proporcionando assim, uma visão integrada do espaço
modelado. Suas classes podem ser georreferenciadas ou convencionais. A distinção
entre classes convencionais e georreferenciadas permite que aplicações diferentes
compartilhem dados não espaciais, facilitando o desenvolvimento de aplicações
integradas e a reutilização de dados.

As classes covencionais são simbolizadas exatamente como na UML. As
classes georreferenciadas são simbolizadas no modelo OMT-G de forma semelhante,
incluindo no canto superior esquerdo um retângulo que é usado para indicar a forma
geométrica da representação. Em ambos os casos, símbolos simplificados podem ser
usados. Os objetos podem ou não ter atributos não espaciais associados, listados na
seção central da representação completa.

A classe geo-objeto com geometria representa objetos que possuem apenas
propriedades geométricas, e é especializada nas classes Ponto, Linha e Polígono,
como por exemplo, respectivamente, árvore, meio fio e edificação (Figura 2.3). A
classe geo-objeto com geometria e topologia representa objetos que possuem, além
das propriedades geométricas, propriedades de conectividade topológica, sendo
especificamente voltadas para a representação de estruturas em rede, como por
exemplo, sistemas de abastecimento de água ou fornecimento de energia elétrica.

\end{comment}

\cite{barbosa:2003}
Os Sistemas de Apoio a Decisão (SAD) têm como objetivo apoiar processos de decisão que apresentam problemas de estruturação. Possuem características tecnológicas, estruturais e de utilização específicas, que os destinguem de outros tipos de Sistemas de Informação (SI); e o seu conhecimento por parte dos seus desenvolvedores.

Segundo Laudon \& Laudon (1999), sistemas de informação representam um conjunto de componentes inter-relacionados trabalhando juntos para coletar, recuperar, processar, armazenar e distribuir informações coma a finalidade de facilitar o planejamento, o controle, a coordenação, a análise e o processo decisório em empresas e outras organizações. Embora não necessite ser baseado em computador, um sistema de informação, que dê suporte ao gerenciamento de negócios e à tomada de decisão, torna-se mais efetivo e estende a capacidade organizacional quando incorporam o apoio computacional.

Há várias classificações para os sistemas de informação. Bidgoli (1998) apresenta três categorias, de acordo com o tipo de atividade a qual apoiam: Sistemas de Processamento de Transação (SPT) - foco nas transações, são voltados para a execução e registro das transações necessárias para se condução o negócio, suportam o nível operacional da organização, onde tarefas, recursos e objetivos são prédefinidos e estruturados. Aspectos importantes a respeito desse tipo de sistema são que eles definem a fronteira entre a organização e o seu meio ambiente e geram de informação para os outros tipos de sistemas;
Sistemas de Informação Gerenciais (SIG) - servem às funções de planejamento, controle e tomada de decisão de nível gerencial. Em geral, condensam informação obtida de SPT e apresentam informações em forma de relatórios sumarizados de rotina e exceção, têm pouca capacidade analítica e usam modelos de apresentação de dados; são orientados quase que exclusivamente para eventos internos; 
Sistemas de Apoio à Decisão (SAD) - foco no suporte às decisões através de simulações com a utilização de modelos; construídos para dar suporte às decisões gerenciais semi-estruturadas ou não-estruturadas, sobre assuntos dinâmicos, que sofrem constantes mudanças de cenário ou que não podem ser facilmente especificados. Apresentam maior capacidade analítica, o que permite empregar vários modelos diferentes para análise de informação. Consideram informações geradas pelos SPT e SIG, bem como de fontes externas.

Segundo Bidgoli (1989) e Mittra (1996), as decisões são classificadas em: Decisão estruturada apresenta procedimento operacional padrão, bem definido e claramente projetado. Este tipo de decisão conta com sistemas de informação relativamente fáceis de definir, programáveis, baseados em lógica clássica, fatos e resultados bem definidos, horizonte de tempo pequeno, rotinas repetitivas e voltados para baixos níveis da organização;
Decisão semi-estruturada: não é totalmente bem definida porém incluem aspectos de estruturação; podem em grande parte contar com apoio dos sistemas de informação;
Decisão não-estruturada: não apresentam qualquer padrão de procedimento operacional, não se repetem. No tocante aos sistemas de informação, estes podem apenas apoiar ao decisor, o qual precisa contar fortemente com sua intuição, experiência etc. São difíceis de formalizar, envolvem heurística, tentativa e erro, senso comum em adição à lógica, horizonte de tempo longo, raramente replica decisões prévias e voltados para os níveis intermediários e alta gerência organização.
Enquanto STP automatiza os processos operacionais, SIG e SAD visam ao suporte a decisões. A diferença básica entre eles é a estrutura da decisão ao qual suportam. Enquanto os SIG são voltados a decisões estruturadas, os SAD visam apoiar decisões semi-estruturadas ou não-estruturadas. É importante perceber que, quanto maior o grau de desestruturação da decisão, maior é a necessidade de interferência do decisor, com sua experiência e feeling para a solução do problema. Os SAD são a principal categoria de interesse deste trabalho.

A arquitetura básica de um SAD inclui dado, modelo e usuário. Vistos na tecnologia da informação, respectivamente, como Banco de Dados, voltado para armazenamento de dados internos ou externos, e mantém associado um SGBD – Sistema Gerenciador de Banco de Dados, software responsável pela manutenção e acesso dos dados na base; Banco de Modelos, incluindo modelos matemáticos e estatísticos, os quais, junto aos dados contidos no Banco de Dados, permitem que sejam feitos os mais variados tipos de análises; e Gerenciador de Diálogos, que deve prover diferentes e amigáveis tipos de interfaces entre o usuário e o sistema. 

Entretanto, para Mittra (1996) a importância das decisões varia de acordo com a situação e com quem as toma. Todas as decisões são baseadas no conhecimento, que é afetado pelas informações. É necessário então, que a informação seja determinada de
acordo com a decisão à qual servirá de apoio.



Para Laudon e Laudon (1999), os sistemas de apoio a decisão são sistemas interativos sob o controle do usuário e que oferecem dados e modelos para a solução de problemas semi-estruturados. 

Um sistema de apoio a decisão é um grupo organizado de pessoas, procedimentos, bancos de dados e dispositivos usados, para dar apoio à tomada de decisões em problemas específicos. É usado quando o problema é complexo e a informação necessária à melhor decisão é difícil de ser obtida e usada (Stair, 1998).

O termo SAD refere-se a uma classe de sistemas que apóiam o processo de decisão em todos os níveis de gerenciamento – operacional, gerencial e estratégico, enfatizando o suporte a decisão e não a sua automação, através de dados e possibilidades de testes de soluções. Representa uma abordagem diferente para apoio a decisões semi-estruturadas e não-estruturadas (Davis, 1974).

O uso adequado de SAD oferece benefícios como o aumento do número de alternativas examinadas para a solução do problema, uma melhor compreensão do negócio, resposta mais rápida a algumas situações inesperadas, possibilidade de desempenhar análises ad hoc, novos conhecimentos e aprendizagens, melhoria na comunicação, melhor controle, melhoria nos custos, melhores decisões, um trabalho de equipe mais eficaz, ganhos de tempo e melhor utilização dos recursos de dados.


As necessidades de informação variam, entre outros, de acordo com o tipo de decisão a ser apoiada. Segundo Bidgoli (1989) e Mittra (1996), as decisões em uma organização podem ser classificadas como:
• Decisão estruturada - apresenta procedimento operacional padrão, bem definido e claramente projetado. Este tipo de decisão conta com sistemas de informação relativamente fáceis de definir, programáveis, baseados em lógica clássica, fatos e resultados bem definidos, horizonte de tempo pequeno, rotinas repetitivas e voltados para baixos níveis da organização;
• Decisão semi-estruturada – situação em que a decisão não é totalmente bem definida,
porém inclui aspectos de estruturação e pode em grande parte contar com apoio dos sistemas de informação;
• Decisão não-estruturada - não apresenta qualquer padrão de procedimento operacional, não se repete. No tocante aos sistemas de informação, estes podem apenas apoiar o decisor, o qual precisa contar fortemente com sua intuição, experiência etc. É difícil de formalizar, envolve heurística, tentativa e erro, senso comum em adição à lógica, horizonte de tempo longo e raramente replica decisões prévias.

\cite{quintella:2003}
Na visão de O’Brien (2001), sistemas de apoio à
decisão podem ser definidos como: “...sistemas de informação computadorizados
que fornecem aos gerentes apoio interativo de informações durante o processo
de tomada de decisão”.
Já Roy, citado por Lupatini (2002), relata que
“o sistema de apoio à decisão é definido como a atividade que permite
através de modelos claramente explicitados, mas não necessariamente
completamente formalizados, ajudar na obtenção de respostas às ques-
tões que são colocadas a um interventor num processo de decisão”.

Nos últimos trinta anos, sugiram poderosos sistemas e ferramentas para
auxiliar o processo de tomada de decisão, dentre elas destacam-se o Data
Warehouse (DW), on-line analytical processing (OLAP) e Data Mining (DM) ou minera-
ção de dados.

O SAD tem ênfase na simulação e explora-
ção de dados, com o objetivo de dar suporte às decisões através de simulações
feitas a partir da utilização de modelos. Estes sistemas têm como usuário principal
o próprio decisor, sendo que seu foco incide na eficácia sobre a tomada de deci-
são.

Decisão é o processo de análise e escolha, entre variáveis alternativas disponíveis para
uma estratégia de ação a se seguir. Segundo Thierauf (1982), os gerentes decidem para estabelecer objetivos, planejar, organizar, direcionar e controlar decisões. A decisão é,
portanto, o centro do processo gerencial.


\cite{pinheiro:2001}

Quando falamos de um sistema de suporte à decisão, a base de dados analítica é apenas
uma parte de um grande sistema. Este sistema inclui ferramentas para armazenar e gerir cubos
multidimensionais (OLAP Server e OLAP Manager), ferramentas para apresentar os dados (Pivot Table Services, Excel, Visual Basic, etc.) e ferramentas para transformar os dados, se
necessário (DTS).

Objectivos de um sistema de suporte à decisão

- Profundidade da informação: Um sistema de suporte à decisão deve permitir aos
utilizadores uma visualização da informação desde os níveis mais altos aos mais
baixos. Assim, o utilizador pode comparar informação de níveis mais elevados com
níveis inferiores conforme as suas necessidades;

- Comparar informação: As comparações mencionadas no parágrafo anterior,
constituem uma funcionalidade essencial num sistema de suporte à decisão: A
capacidade de comparar conjuntos de informação. Por exemplo, uma das
comparações mais comuns consiste no volume de vendas num determinado período
comparada com outro período. Um bom sistema de suporte à decisão deve
proporcionar facilidade na execução deste tipo de comparações;

- Informação útil: Ser capaz de extrair grandes quantidades de informação é inútil
se esta não for precisa e aplicável aos problemas de negócio que necessitam de
resolução. Para além disso, os sistemas devem ser construídos por forma a validar a
informação suficiente que permita ao utilizador efectuar as decisões que beneficiem
a organização;

- Disponibilidade da informação: Não é recomendável ter um sistema que demore
dias ou semanas para mover a informação para a base de dados analítica, quando é
necessário disponibilizá-la em algumas horas. Se a informação é correcta e
pertinente, mas chega demasiado tarde à organização, torna-se inútil;

- Análise rápida: Para satisfazer as necessidades dos utilizadores, cada query deve
ser efectuado rapidamente, de forma a que um utilizador possa efectuar vários
queries num curto espaço de tempo;

- Informação acessível: A informação deve ser apresentada num linguagem
perceptível no âmbito do negócio, e não numa linguagem específica de sistemas
analíticos. O interface deve permitir aos utilizadores uma boa interacção através
uma linguagem e termos que lhe sejam familiares.

\cite{obrien:2013}
Decisão estruturada: envolve situações que permitem especificação antecipada dos procedimentos a serem seguidos. 
Decisão não estruturada: situações que não permitem a especificação antecipada da maioria dos procedimentos a serem seguidos. (Ex: decisões de longo prazo)
Decisões semiestruturadas: é possível especificar antecipadamente alguns procedimentos, mas não o suficiente para conduzir a uma decisão definitiva. (decisões empresariais)


SAD: são sistemas de informação baseados em computador que oferecem informações interativas a gerentes e profissionais de negócios durante o processo decisório. Os sistemas de apoio a decisão usam (1) modelos analíticos, (2) bancos de dados especializados, (3) opnião e percepção do próprio responsável pela decisão e (4) um processo interativo de modelagem baseada em computador para apoiar a tomada de decisão empresarial semiestruturada.

\end{comment}


Rede de sensores - Referências

[9] I. F. Akyildiz, W. Su, Y. Sankarasubramaniam, e E. Cayirci. Wireless Sensor Networks: a Survey. Computer Networks, vol. 38, p. 393-422, 2002.

[10] A. A. F. Loureiro. Redes de sensores sem fio. Em Grandes Desafios da Pesquisa em Computação para o perı́odo 2006-2016. Universidade Federal de Minas Gerais, 2006.

[11] F. A. Silva, T. R. de M. Braga, e L. B. Ruiz. Tecnologia de nós sensores sem fio, Agosto de 2004. Disponı́vel http://homepages.dcc.ufmg.br/~linnyer/ufmgnossensores.pdf. Acesso em: 01 de Outubro de 2013.

[12] L. B. Ruiz. MANNA: Uma Arquitetura para Gerenciamento de Redes de Sensores Sem Fio. Dissertação de Doutorado, Departamento de Ciência da Computação da Universidade Federal de Minas Gerais, 2003.

[13] L.B. Ruiz, L. H. A. Correia, L. F. M. Vieira, D. F. Macedo, E. F. Nakamura, C. M. S. Figueiredo, E. H. B. Maia, M. A. M. Vieira, J. M. S. Nogueira, A. A. F. Loureiro, D. C. da Silva, e A. O. Fernandes. Arquiteturas para Redes de Sensores Sem Fio. Em XXII Simpósio Brasileiro de Redes de Computadores, 2004.

[14] A.A.F. Loureiro, J. M. S. Nogueira, L. B. Ruiz, R. A. de F. Mini, E. F. Nakamura, e C. M. S. Figueiredo. Redes de Sensores Sem Fio. Em XXI Simpósio Brasileiro de Redes de Computadores, 2003.

[16] P. B. Velloso. Transmissão de Voz em Redes Ad Hoc. Dissertação de Mestrado, Universidade Federal do Rio de Janeiro, Agosto de 2003.

[17] M. A. Perillo e W. B. Heinzelman. Fundamental Algorithms and Protocols for Wireless and Mobile Networks, chapter Wireless Sensor Network Protocols, p. 813 – 842. CRC Hall, 2005.

[19] I. Dietrich e F. Dressler. On the Lifetime of Wireless Sensor Networks. ACM Transactions on Sensor Networks, vol. 5, no. 1, artigo no. 5, p. 1 - 39, Fevereiro de 2009.

[22] E.C.R. de Oliveira e C.V.N. de Albuquerque. Avaliação de protocolos de roteamento para redes ad-hoc e RSSF aplicados à TV digital interativa e cidades digitais. Em XXXIII Conferencia Latinoamericana de Informatica (CLEI 2007), San Jose, Costa Rica, Outubro de 2007.

[26] L. Pelusi, A. Passarella, e M. Conti. Opportunistic Networking: Data Forwarding in Disconnected Mobile Ad hoc Networks. Communications Magazine, IEEE, vol. 44, p. 134-141, Novembro de 2006.

[27] L. Lilien, Z. H. Kamal, V. Bhuse, e A. Gupta. Opportunistic networks: The concept and research challenges in privacy and security. Relatório Técnico, Department of Computer Science Western Michigan University, Kalamazoo, 2005.


\begin{comment}
\cite{laudon:2010}

Os Sistemas de Identificação por Radiofrequência (RFID \textit{Radio Frequency Identification} usam minúsculas etiquetas com microchips embutidos com dados sobre um item e sua localização para transmitir sinais de rádio a curta distância para leitores RFID. Os leitores RFID repassanm, então, os dados por rede a um computador que os processa. Diferentemente dos códigos de barra, as etiquetas RFID não precisam estar na linha de visão da leitora para serem reconhecidas.

Embutido na etiqueta está um chip que armazena esses dados (localização, onde fo fabricado, ...). O restante da etiqueta é uma antena que transmite os dados para o leitor.

A unidade leitora consiste em uma antena e um transmissor de rádio com função de decodificação, anexados a um dispositivo de mão ou estacionário. A leitora emite ondas de rádio em um raio que vai de 2,5 centímetros até 30 metros, dependendo de sua potência de saída, da frequência de radio empregada e das condições do ambiente circundante. Quando um etiqueta RFID entra no raio do leitor, essa etiqueta é ativada e começa a enviar dados. O leitor captura tais dados, decodifica-os e envia-os de volta por uma rede sem fio ou cabeada até um computador hospedeiro, para posterior processamento. Tanto as antenas quantos as etiquetas podem ter vários formatos e tamanhos.

As etiquetas RFID ativas são alimentadas por uma bateria interna e costumam permitir que os dados sejam regravados e modificados. Etiquetas RFID passivas não possuem fonte própria de energia e obtêm seu poder operacional da energia por radiofrequência transmitida pelo leitor RFID. São menores, mais leves e mais baratas do que as etiquetas ativas, mas alcançam somente aluguns metros.

Redes de Sensores sem Fio (WSNs - \textit{Wireless Sensor Networks}) são redes de dispositivos sem fio interconectados e introduzidos no ambiente físico para fornecer medições de vários pontos em grandes espaços. Elas se baseiam em dispositivos com sensores e antenas de radiofrequência, armazenamento eprocessamento embutidos. Esses dispositivos se unem por uma rede interconectada que direciona os dados capturados por eles para um computador que fará a análise.

Essas redes possuem desde centenas até milhares de nós. Como os dispositivos sensores sem fio ficam anos instalados, sem manutenção ou interferência humana, precisam ter consumo de energia baixúissimo e baterias capazes de resistir longos períodos - por anos, na verdade.

As redes de sensores sem fio são muito úteis em áreas como monitoramento de mudanças ambientais; monitoramento de tráfego ou atividade militar; proteção de propriedade; operação e gestãoeficiente de máquinas e veículos; estabeleciemnto de perímetros de segurança; monitoramento da gestão da cadeia de suprimento,; e detecção de materiais radioativos, biológicos ou químicos.

\end{comment}


\begin{comment}
\cite{oliveira:2013}

Sendo componente principal desta tecnologia, um sensor é um aparelho que produz uma resposta a uma alteração mensurável em uma condição fı́sica. Logo, ele pode ser projetado para medir a temperatura, a pressão do ar, a intensidade de luz, entre outras grandezas fı́sicas.

Conceitualmente, uma Rede de Sensores Sem Fio (RSSF) consiste em um grande número dispositivos compactos e autônomos (chamados nós sensores) densamente distribuı́dos em uma região de interesse [10, 11]. Uma RSSF é utilizada como uma ferramenta de sensoriamento distribuı́do de fenômenos (entidades de interesse que estão sendo monitorados), processamento e disseminação de dados coletados e informações processadas para um ou mais observadores (usuário final interessado pela informação) [12].

Em uma RSSF, cada nó é capaz de realizar as seguintes funções: sensoriamento do ambiente, processamento da informação, armazenamento de informação e comunicação com outros sensores através do canal de comunicação sem fio. 

%Por outro lado, isso também significa que os protocolos de rede de sensores e os algoritmos devem possuir capacidades de auto-organização.

A comunicação entre uma RSSF e outras redes ocorre por meio de nós ponto de acesso (gateway). O ponto de acesso pode ser implementado em um nó da rede que será conhecido como nó sorvedouro (sink node) ou em uma estação base (base station). As informações coletadas percorrem a RSSF até chegar a um ponto de acesso que irá encaminhá-las, por uma rede como a Internet, até a máquina onde estará a aplicação que utilizará as informações [11, 13].

Redes de Sensores Sem Fio diferem das redes de computadores convencionais em diversos aspectos. Elas possuem um grande número de nós distribuı́dos, têm restrições de energia, devem possuir mecanismos para autoconfiguração e adaptação devido a problemas como falhas de comunicação e perda de nós [14].

Em várias situações, o ambiente a ser monitorado não possui uma infraestrutura necessária para suportar conexões cabeadas fim-a-fim (end-to-end ). Dessa forma, os sensores devem usar os canais de comunicação sem fio (wireless) para solucionar esta adversidade. As redes sem fio podem ser divididas em duas distintas categorias. A primeira abrange as redes com infraestrutura, nas quais toda a comunicação é realizada através de um ponto de acesso. A segunda categoria engloba as redes sem infraestrutura (ad-hoc), nas quais as estações se comunicam diretamente, não existindo um ponto de acesso [16].

Em comparação com as redes ad-hoc tradicionais, as principais diferenças de uma rede de sensores são: o número de nós em uma rede de sensores pode ser muito maior do que os nós de uma rede ad-hoc tradicional, os nós sensores são densamente implantados, os nós sensores são propensos a falhas, a topologia de uma rede de sensores muda frequentemente, os nós sensores utilizam principalmente o paradigma de comunicação broadcast enquanto a maioria das redes ad-hoc tradicional são baseadas na comunicação ponto-a-ponto, os nós sensores possuem limitações de potência e capacidades computacionais, e por fim, os nós sensores podem não possuir uma identificação global devido à grande quantidade de sobrecarga e o alto número de sensores [17].

A comunicação de múltiplos saltos (multi-hop) é uma forma de comunicação de dados presente nas RSSFs. Neste tipo de comunicação, caso um sensor (nó origem) precise transmitir os dados de sua memória para uma estação base ou outro sensor da rede (nó destino) fora de seu alcance de transmissão, ele transmitirá os dados para um sensor mais próximo que, caso o destino esteja fora de seu alcance, transmitirá a outro sensor até alcançar o destino. A transmissão de dados de um sensor para outro é conhecida como salto (hop). Logo, em uma rede de sensores multi-hop, os nós atuam em dois papéis: criador e roteador de dados [9].

O método de comunicação multi-hop é uma das principais formas de economia no consumo de energia nas redes de sensores sem fio.

Como os sensores utilizam energia provinda de uma bateria embarcada, eles possuem uma quantidade limitada de recursos energéticos, os quais determinam seu tempo de vida. O tempo de vida de uma RSSF depende fortemente dos tempos de vida dos nós que constituem a rede. A vida útil de um nó sensor depende basicamente de dois fatores: quanto de energia ele consome ao longo do tempo e o quanto de energia está disponı́vel para uso [19].

O processo de roteamento em uma RSSF consiste na escolha entre os vários caminhos possı́veis para se enviar os dados coletados pelos sensores. O nó destino poderá ser o nó que processará os dados ou um ponto de acesso que enviará os dados para a aplicação em questão.

Nas Redes Tradicionais, o roteamento segue o modelo Centrado em Endereços (Address-Centric), onde o foco está na busca pelo menor caminho entre pares (origem e destino) de nós endereçáveis. Nesse modelo, cada nó envia seus dados ao nó sorvedouro de forma independente, ou seja, sem a necessidade de uma requisição do sorvedouro para que o nó envie os dados. Já nas Redes de Sensores, a abordagem mais usada é a Centrada em Dados (Data-Centric), onde a estação base envia consultas para regiões especı́ficas da RSSF e aguarda pelos dados dos sensores localizados nas regiões selecionadas. O modelo ainda permite o processo sumarização, evitando assim a redundância dos dados [13, 22].

No roteamento hierárquico, os nós sensores são organizados em grupos (clusters). Em cada grupo, existem dois tipos de nós: os nós fontes e o lı́der de grupo (cluster head ). 

No Roteamento Plano (Flat Routing), todos os sensores são considerados iguais, ou seja, a atividade de roteamento é tratada de forma idêntica por todos os sensores da rede.

Apesar de providenciar eficientes metodologias para comunicação em RSSFs, os protocolos de Roteamento Hierárquico e Plano restringem-se apenas às redes nas quais os sensores permanecem estáticos no ambiente. Em uma rede na qual os nós sensores possuem mobilidade, determinar uma topologia é inviável e, logo, o emprego de protocolos das duas famı́lias de roteamento também.

A solução para isso fica por conta do uso de algoritmos de Redes Oportunı́sticas (Opportunistic Networks). Segundo Perusi et al. [26], uma Rede Oportunı́stica é uma evolução das redes ad-hoc multi-hop. Já para Lilien et al. [27], elas podem ser compreendidas como redes que estão dentro de uma intersecção formada pelas Redes Ad hoc, Sistemas P2P (Peer-to-peer ) e Redes de Sensores.

Nesse tipo de rede, nenhuma suposição é feita a respeito da existência de um caminho completo entre dois nós. Nós origem e destino podem nunca estar conectados com a mesma rede, ao mesmo tempo. Entretanto, as técnicas de rede oportunistas permitem tais nós trocarem mensagens entre si.
\end{comment}
