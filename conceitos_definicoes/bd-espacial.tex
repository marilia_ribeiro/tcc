\subsection{Banco de Dados Espaciais e Geográficos} \label{section:bd_espacial}

Um Sistema de Informação Geográfico (SIG) pode ser definido como uma coleção de programas independentes que permitem a aquisição, criação, vizualização, consulta e análise de dados geoespaciais. Um SIG é capaz de armazenar tanto atributos descritivos, quanto atributos geométricos de diferentes tipos de dados geográficos. Essa é a principal característica que o diferencia de um SI convencional \cite{fitz:2008, queiroz-ferreira:2006, silberschatz:1999}. 

De acordo com \cite{fitz:2008} as funcionalidades de um SIG estão relacionadas à estrutura do sistema ao qual estão vinculados. No entanto, em geral, as principais funcionalidades são: aquisição e edição de dados, gerenciamento do bando de dados, análise geográfica de dados, e representação de dados. 

Bancos de dados geográficos (BDG) ou banco de dados espaciais (BDE), são utilizados para armazenar dados que representam o espaço físico, geográfico e espacial. Por exemplo, mapas, imagens de satélite, dados de localização como coordenadas geográficas, objetos como prédios, hidrantes, pontos turísticos, entre outros. Em algumas literaturas os BDGs são chamados de SIG \cite{silberschatz:1999}.

As estruturas de dados suportadas por BDG são as matriciais e as vetoriais. A estrutura matricial trata o espaço como uma superfície plana, e divide a informação em grades, onde cada célula representa uma porção do terreno. Já a estrutura vetorial é utilizada para representar as fronteiras das entidades geográficas. Essa representação é dada por meio de pontos, linhas e polígonos, definidos através de suas coordenas \cite{queiroz-ferreira:2006}. 

O ponto é representado por um par ordenado $(x, y)$ de coordenadas geográficas. A linha é representada por um conjunto de pontos conectados. E o polígono é uma região do plano delimitada por linhas conectadas por dois ou mais pontos, em que o útimo ponto deve ser igual ao primeiro. Além disso, assim como em um BD convencioal, é possível determinar relações entre esses elementos. Existem vários tipos de relações, entre elas as mais comuns são: adjacência, pertinência, intersecção e cruzamento \cite{queiroz-ferreira:2006}.

A Figura \ref{figura:estrutura_vetorial} apresenta o mapa da Escola Fazenda do IFC Araquari que é a região do estudo de caso deste trabalho. Nesta imagem é possível identificar os elementos das estruturas de dados citadas a cima. O elemento representado pelo número 1 (em azul) apresenta a grade da estrutura matricial. Os elementos da estrutura vetorial podem ser identidicados pelos números 2, 3 e 4 que representam, respectivamente, pontos (amarelo), linhas (laranja) e polígonos (verde). 

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth,height=0.33\textheight]{imagens/qgis_grid_2.png}
	\caption{Mapa da Escola Fazenda do IFC Araquari mostrando os elementos da estrutura matricial e vetorial de um BDG.}
	\label{figura:estrutura_vetorial}
\end{figure}
%\FloatBarrier

As relações citadas, também podem ser observadas na Figura \ref{figura:estrutura_vetorial}. A relação de adjacência pode ser observada pela linha localizada na região superior do mapa. A pertinência pode ser observada pelos pontos localizados dentro do polígono. As relações de intersecção e o cruzamento podem ser observadas pelas linhas (que representam ruas) localizadas na parte inferior do mapa.

A seguir, são definidos os conceitos aplicados no campo de estudo de trajetórias que serão utilizados para o desenvolvimento deste trabalho. Estas definições são baseadas nos trabalhos de \citeonline{fontes_and_bogorny:2013}, \citeonline{zheng-etal:2009}, \citeonline{vincenty:1975} e \citeonline{melo:2016}:

\begin{itemize}
\item \label{coordenada} \textbf{Coordenada ($c$)}: é uma tupla ($x, y$), tal que $x$ é a latitude e $y$ é a longitude. 

\item \label{ponto} \textbf{Ponto ($p$)}: é uma tupla ($c, t$), tal que $c$ é uma coordenada e $t$  representa o instante de tempo em que a coordenada  foi capturada.

\item \label{trajetoria} \textbf{Trajetória ($T$)}: é composta por um vetor de pontos e pode ser definida da seguinte maneira $T = [p_{1}, p_{2},  \ldots ,p_{n}]$, tal que $p_{1}$ é o ponto inicial, $p_{n}$ é o ponto final e $n$ é o seu número de pontos. 

\item \label{sub-trajetoria} \textbf{Sub-trajetória ($ST$)}: é um vetor $ST = [p_{i}, p_{i+1}, p_{i+2}, \ldots ,p_{f}]$ , tal que $p_{i}$ é o ponto inicial e $p_{f}$  é o ponto final. Além disso, uma sub-trajetória é um subconjunto de uma trajetória, pois $ST \subset T$.

\item \label{azimute} \textbf{Azimute ($AZ$)}: é o ângulo horizontal medido de forma horária entre a direção norte-sul do meridiano que passa pelo ponto atual e um segmento entre esse ponto e o ponto seguinte. Este ângulo vaira entre 0$^{\circ}$ e 360$^{\circ}$. A Figura \ref{figura:azimute} mostra essa angulação.
\end{itemize}

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth,height=0.2\textheight]{imagens/azimute.png}
    \caption{Representação do azimute.}
    \fonte{\citeonline{melo:2016}.}
    \label{figura:azimute}
\end{figure}

Existem duas abordagens para o cálculo do azimute: o plano e o geodésico. Neste trabalho utiliza-se o azimute geodésico. As fórmulas para esse cálculo podem ser encontradas em \citeonline{vincenty:1975}.

\begin{itemize}
\item \label{deflexao-angular} \textbf{Deflexão ($DF$)}: é ângulo formado entre o prolongamento do segmento anterior e o segmento atual, contado para a direita ou para a esquerda. Este ângulo vaira entre 0$^{\circ}$ e 180$^{\circ}$.
\end{itemize}

Neste trabalho, assim como no trabalho de \citeonline{melo:2016}, o cálculo de deflexão adotado é dado pela diferença entre dois azimutes consecutivos, como mostra a Equação \ref{equacao:deflexao}. Essa diferença é positiva se o primeiro azimute da sequência for maior que o segundo, caso contrário, é negativa. No entanto, no método proposto por \citeonline{melo:2016} o sinal é desconsiderado através do módulo presente na Equação \ref{equacao:deflexao}.


\begin{equation}\label{equacao:deflexao}  
    DF_{i} = |(AZ_{i} - AZ_{i+1})|   
\end{equation}



Onde, $AZ_{i}$ representa o azimute entre dois pontos dentro de uma trajetória e $AZ_{i+1}$ o azimute subsequente entre dois pontos, também dentro da mesma trajetória. 

\begin{itemize}
\item \label{vetor-deflexao} \textbf{Vetor de Deflexão ($\overrightarrow{DF}$)}: é um vetor definido como $ \overrightarrow{DF} = [DF_{i}, DF_{i+1}, DF_{i+2}, ..., DF_{k}] $, onde $0 < i < k$ e $i < k \le j-1$, $j$  é o número de azimutes da trajetória.
\end{itemize}

\begin{comment}
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth,height=0.25\textheight]{imagens/deflexao_angular.png}
    \caption{Representação do cálculo de deflexão.}
    Fonte: \citeonline{melo:2016}.
    \label{figura:azimute}
\end{figure}
\end{comment}

